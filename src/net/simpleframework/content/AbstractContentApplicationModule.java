package net.simpleframework.content;

import net.simpleframework.organization.EJobType;
import net.simpleframework.organization.IJob;
import net.simpleframework.organization.IJobChart;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.web.AbstractWebApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class AbstractContentApplicationModule extends AbstractWebApplicationModule
		implements IContentApplicationModule {

	// 创建管理员角色
	protected void createManagerJob(final String manager, final String msanagerText) {
		IJob job = OrgUtils.jm().getJobByName(manager);
		if (job == null) {
			final IJobChart jc = OrgUtils.jcm().getJobChartByName(IJobChart.sysjc);
			if (jc != null) {
				job = OrgUtils.jm().createJob(jc, manager, msanagerText);
				job.setJobType(EJobType.normal);
				OrgUtils.jm().insert(job);
			}
		}
	}

	@Override
	public String getCatalogIdName(final PageRequestResponse requestResponse) {
		return "catalog";
	}

	private String pagerHandleClass;

	@Override
	public String getPagerHandleClass() {
		return pagerHandleClass;
	}

	public void setPagerHandleClass(final String pagerHandleClass) {
		this.pagerHandleClass = pagerHandleClass;
	}

	@Override
	public String tabs2(final PageRequestResponse requestResponse) {
		return null;
	}
}
