package net.simpleframework.sysmgr.dict;

import net.simpleframework.ado.DataObjectManagerUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.ado.db.Table;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.PageRequestResponse;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class DictUtils {
	public static ISysDictApplicationModule applicationModule;

	public static String deployPath;

	public static String getCssPath(final PageRequestResponse requestResponse) {
		final StringBuilder sb = new StringBuilder();
		sb.append(deployPath).append("css/").append(applicationModule.getSkin(requestResponse));
		return sb.toString();
	}

	public static ITableEntityManager getTableEntityManager(final Class<?> beanClazz) {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule, beanClazz);
	}

	public static ITableEntityManager getTableEntityManager() {
		return DataObjectManagerUtils.getTableEntityManager(applicationModule);
	}

	public static SysDict getSysDictById(final Object id) {
		if (id == null) {
			return null;
		}
		return getTableEntityManager(SysDict.class).queryForObjectById(id, SysDict.class);
	}

	public static SysDict getSysDictByName(final String name) {
		final String[] nameArr = StringUtils.split(name, ".");
		if (nameArr == null || nameArr.length == 0) {
			return null;
		}
		final ITableEntityManager temgr = getTableEntityManager(SysDict.class);
		final SysDict sysDict = temgr.queryForObject(
				new ExpressionValue(Table.nullExpr(temgr.getTable(), "documentid") + " and name=?",
						new Object[] { nameArr[0] }), SysDict.class);
		if (sysDict == null) {
			return null;
		}
		if (nameArr.length == 1) {
			return sysDict;
		} else {
			return temgr.queryForObject(new ExpressionValue("documentid=? and name=?", new Object[] {
					sysDict.getId(), nameArr[1] }), SysDict.class);
		}
	}
}
