package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class TransitionNode extends Node {

	private AbstractTransitionType transitionType;

	private String from, to;

	public TransitionNode(final Element element, final ProcessNode processNode) {
		super(element == null ? addNode(processNode, "transition") : element, processNode);
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(final String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(final String to) {
		this.to = to;
	}

	public AbstractTransitionType getTransitionType() {
		if (transitionType == null) {
			transitionType = new AbstractTransitionType.Conditional(null, this);
		}
		return transitionType;
	}

	@Override
	public void syncElement() {
		super.syncElement();
		getTransitionType().syncElement();
	}

	@Override
	public void parseElement() {
		super.parseElement();
		getTransitionType().parseElement();
	}
}
