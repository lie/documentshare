package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class AbstractProcessParticipantType extends AbstractParticipantType {

	public AbstractProcessParticipantType(final Element element, final AbstractNode parent) {
		super(element, parent);
	}

	public static class Job extends AbstractProcessParticipantType {

		public Job(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "job") : element, parent);
		}
	}

	public static class User extends AbstractProcessParticipantType {

		public User(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "user") : element, parent);
		}
	}
}
