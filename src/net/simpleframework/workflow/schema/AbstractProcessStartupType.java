package net.simpleframework.workflow.schema;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class AbstractProcessStartupType extends AbstractNode {
	public AbstractProcessStartupType(final Element element, final ProcessNode processNode) {
		super(element, processNode);
	}

	public static class Manual extends AbstractProcessStartupType {
		private AbstractProcessParticipantType participantType;

		public Manual(final Element element, final ProcessNode processNode) {
			super(element == null ? addStartupType(processNode, "manual") : element, processNode);
		}

		public AbstractProcessParticipantType getParticipantType() {
			if (participantType == null) {
				participantType = new AbstractProcessParticipantType.User(null, this);
			}
			return participantType;
		}

		public void setParticipantType(final AbstractProcessParticipantType participantType) {
			this.participantType = participantType;
		}

		@Override
		public void syncElement() {
			super.syncElement();
			getParticipantType().syncElement();
		}

		@Override
		public void parseElement() {
			super.parseElement();
			final Element ele = getElement().element("participant-type");
			if (ele == null) {
				return;
			}
			Element ele2;
			if ((ele2 = ele.element("job")) != null) {
				participantType = new AbstractProcessParticipantType.Job(ele2, this);
			} else if ((ele2 = ele.element("user")) != null) {
				participantType = new AbstractProcessParticipantType.User(ele2, this);
			}
		}
	}

	public static class Email extends AbstractProcessStartupType {
		public Email(final Element element, final ProcessNode processNode) {
			super(element == null ? addStartupType(processNode, "email") : element, processNode);
		}
	}

	static Element addStartupType(final ProcessNode processNode, final String name) {
		return addElement(processNode, "startup-type", name);
	}
}
