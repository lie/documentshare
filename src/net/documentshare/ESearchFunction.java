package net.documentshare;

public enum ESearchFunction {
	docu() {
		@Override
		public String toString() {
			return "文档";
		}

		@Override
		public String toUrl() {
			return "/docusearch.html";
		}
	},

	news {
		@Override
		public String toString() {
			return "资讯";
		}

		@Override
		public String toUrl() {
			return "/news.html";
		}
	},

	blog {
		@Override
		public String toString() {
			return "博客";
		}

		@Override
		public String toUrl() {
			return "/blog.html";
		}
	}/*,

	question {
		@Override
		public String toString() {
			return "问答";
		}

		@Override
		public String toUrl() {
			return "/question.html";
		}
	},

	bbs {
		@Override
		public String toString() {
			return "论坛 ";
		}

		@Override
		public String toUrl() {
			return "/bbs/tl.html";
		}
	}*/;

	public String toUrl() {
		return null;
	}

	public static ESearchFunction whichSearch(final String name) {
		try {
			return ESearchFunction.valueOf(name);
		} catch (Exception e) {
		}
		return ESearchFunction.docu;
	}
}
