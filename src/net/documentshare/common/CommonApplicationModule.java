package net.documentshare.common;

import java.util.Map;

import net.documentshare.impl.AItSiteAppclicationModule;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.IInitializer;
import net.simpleframework.core.ado.db.Table;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.AbstractComponentBean;

public class CommonApplicationModule extends AItSiteAppclicationModule implements ICommonApplicationModule {
	private String deployName = "common";

	@Override
	public void init(IInitializer initializer) {
		try {
			super.init(initializer);
			doInit(CommonUtils.class, deployName);
		} catch (Exception e) {
		}
	}

	static final Table it_common = new Table("it_common");

	@Override
	public Class<?> getEntityBeanClass() {
		return CommonBean.class;
	}

	@Override
	protected void putTables(Map<Class<?>, Table> tables) {
		super.putTables(tables);
		tables.put(CommonBean.class, it_common);
	}

	@Override
	public AbstractComponentBean getComponentBean(PageRequestResponse requestResponse) {
		return null;
	}

	@Override
	public String getViewUrl(Object id) {
		final StringBuilder sb = new StringBuilder();
		return sb.toString();
	}

	@Override
	public ITableEntityManager getDataObjectManager() {
		return super.getDataObjectManager();
	}

	@Override
	public String getDeployPath() {
		return null;
	}

	@Override
	public CommonBean getCommonBean(ECommonType type) {
		final ITableEntityManager tMgr = getDataObjectManager();
		return tMgr.queryForObject(new ExpressionValue("type=?", new Object[] { type }), CommonBean.class);
	}

}
