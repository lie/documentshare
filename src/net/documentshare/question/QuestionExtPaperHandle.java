package net.documentshare.question;

import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class QuestionExtPaperHandle extends AbstractPagerHandle {

	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("title".equals(beanProperty)) {
			final int counter = getCount(compParameter);
			if (counter > 0)
				return "共有<span class='_red'>" + counter + "</span>条问答补充说明";
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(ComponentParameter compParameter) {
		final ITableEntityManager tMgr = QuestionUtils.applicationModule.getDataObjectManager(QuestionExtBean.class);
		final String questionId = compParameter.getRequestParameter("questionId");
		return tMgr.query(new ExpressionValue("questionId=" + questionId + " order by createDate desc"), QuestionExtBean.class);
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "questionId");
		return parameters;
	}

}
