package net.documentshare.question;

import java.io.File;
import java.io.IOException;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.content.AbstractContentLuceneManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.bean.IDataObjectBean;
import net.simpleframework.util.BeanUtils;
import net.simpleframework.web.page.component.ComponentParameter;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;

public class QuestionLuceneManager extends AbstractContentLuceneManager {
	public QuestionLuceneManager(final ComponentParameter compParameter, final File indexPath) {
		super(compParameter, indexPath);
	}

	@Override
	protected String[] getQueryParserFields() {
		return new String[] { "title", "content" };
	}

	@Override
	protected void objectToDocument(final Object object, final IndexWriter indexWriter, final Document doc) throws IOException {
		super.objectToDocument(object, indexWriter, doc);
		for (final String field : getQueryParserFields()) {
			try {
				final Object value = BeanUtils.getProperty(object, field);
				if (value != null) {
					doc.add(new Field(field, String.valueOf(value), Store.NO, Index.ANALYZED));
				}
			} catch (final Exception e) {
				logger.warn(e);
			}
		}
	}

	@Override
	protected IDataObjectBean queryForObject(Object id) {
		return QuestionUtils.applicationModule.getBean(QuestionBean.class,id);
	}

	@Override
	protected IDataObjectQuery<?> getAllData() {
		final ITableEntityManager tMgr = QuestionUtils.applicationModule.getDataObjectManager();
		return tMgr.query(new ExpressionValue("1=1"), QuestionBean.class);
	}
}
