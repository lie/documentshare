package net.documentshare.question.remark;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.documentshare.question.QuestionBean;
import net.documentshare.question.QuestionUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.OrgUtils;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.DateUtils;
import net.simpleframework.util.HTMLBuilder;
import net.simpleframework.util.LocaleI18n;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.PagerUtils;

public abstract class QuestionRemarkUtils {
	public static String deployPath;
	public static IQuestionRemarkApplicationModule applicationModule;

	public static String itemsHtml(final ComponentParameter compParameter) {
		final QuestionBean questionBean = QuestionUtils.applicationModule
				.getBean(QuestionBean.class, compParameter.getRequestParameter("questionId"));
		return itemsHtml(compParameter, PagerUtils.getPagerList(compParameter.request), questionBean == null ? false : questionBean.isOpen());
	}

	private static String itemsHtml(final ComponentParameter compParameter, final List<?> items, final boolean open) {
		if (items == null) {
			return "";
		}
		final String questionId = compParameter.getRequestParameter("questionId");
		final QuestionBean questionBean = QuestionUtils.applicationModule.getBean(QuestionBean.class, questionId);
		if (questionBean == null) {
			return "";
		}
		final StringBuilder sb = new StringBuilder();
		for (final Object object : items) {
			final QuestionRemarkItem item = (QuestionRemarkItem) object;
			sb.append("<div class='item " + (item.isGoodAnswer() ? "goodAnswer" : "") + "'>");
			sb.append("<div class='d1' style='margin-right: 12px;'>");
			sb.append("<table><tr><td nowrap='nowrap'>");
			sb.append("<span class=\"ut\">").append(
					StringUtils.text(ContentUtils.getAccountAware().wrapAccountHref(compParameter, item.getUserId()),
							LocaleI18n.getMessage("remark.3")));
			sb.append(" (").append(item.getIp()).append(")</span>, ");
			sb.append("<span class=\"cd\">").append(DateUtils.getRelativeDate(item.getCreateDate()));
			sb.append("</span>");
			sb.append("</td>");
			if (item.getParentId().equals2(0) && open) {
				sb.append("<td align='right' width='100%'>");
				if (ItSiteUtil.isManageOrSelf(compParameter, QuestionUtils.applicationModule, questionBean.getUserId()))
					sb.append("<a onclick=\"$Actions['setGoodAnswerAct']('itemId=" + item.getId() + "&questionId=" + questionId + "');\">设为最佳答案</a>");
				sb.append("</td>");
			}
			sb.append("</tr></table>");
			sb.append("</div>");
			sb.append("<div class='inherit_c wrap_text'>").append(item.getContent()).append("</div>");
			sb.append("<div class='d2'>");

			final ArrayList<String> al = new ArrayList<String>();
			final IAccount account = AccountSession.getLogin(compParameter.getSession());
			if (account != null) {
				final String jobEdit = "true";
				if (OrgUtils.isMember(jobEdit, account.user()) || isRemarkEdit(account, item)) {
					final StringBuilder sb2 = new StringBuilder();
					sb2.append("<a onclick=\"__remark_window(this, 'itemId=");
					sb2.append(item.getId()).append("');\">");
					sb2.append(LocaleI18n.getMessage("remark.7"));
					sb2.append("</a>");
					al.add(sb2.toString());
				}
				final String jobDelete = "true";
				if (OrgUtils.isMember(jobDelete, account.user())) {
					final StringBuilder sb2 = new StringBuilder();
					sb2.append("<a onclick=\"$Actions['ajaxRemarkDelete']('itemId=");
					sb2.append(item.getId()).append("');\">");
					sb2.append(LocaleI18n.getMessage("remark.8"));
					sb2.append("</a>");
					al.add(sb2.toString());
				}
			}
			final IDataObjectQuery<QuestionRemarkItem> qs = QuestionRemarkUtils.applicationModule.getRemarkItems(compParameter, item.getId());
			final List<QuestionRemarkItem> items2 = new ArrayList<QuestionRemarkItem>();
			QuestionRemarkItem item2;
			while ((item2 = qs.next()) != null) {
				items2.add(item2);
			}

			if (true) {
				final StringBuilder sb2 = new StringBuilder();
				sb2.append("<a onclick=\"__remark_window(this, 'parentId=");
				sb2.append(item.getId()).append("');\">");
				sb2.append(LocaleI18n.getMessage("remark.4"));
				sb2.append("</a><label>");
				if (items2.size() > 0) {
					sb2.append("(").append(items2.size()).append(")");
				}
				sb2.append("</label>");
				al.add(sb2.toString());
			}

			if (true) {
				StringBuilder sb2 = new StringBuilder();
				sb2.append("<a onclick=\"$Actions['ajaxRemarkSupport']('itemId=");
				sb2.append(item.getId()).append("');\">");
				sb2.append(LocaleI18n.getMessage("remark.5"));
				sb2.append("</a><label id='remark_support_").append(item.getId()).append("'>");
				if (item.getSupport() > 0) {
					sb2.append("(").append(item.getSupport()).append(")");
				}
				sb2.append("</label>");
				al.add(sb2.toString());
				sb2 = new StringBuilder();
				sb2.append("<a onclick=\"$Actions['ajaxRemarkOpposition']('itemId=");
				sb2.append(item.getId()).append("');\">");
				sb2.append(LocaleI18n.getMessage("remark.6"));
				sb2.append("</a><label id='remark_opposition_").append(item.getId()).append("'>");
				if (item.getOpposition() > 0) {
					sb2.append("(").append(item.getOpposition()).append(")");
				}
				sb2.append("</label>");
				al.add(sb2.toString());
			}
			if (open)
				sb.append(StringUtils.join(al, HTMLBuilder.SEP));
			sb.append("</div>");
			if (items2.size() > 0) {
				sb.append("<div class='d3'>");
				sb.append(itemsHtml(compParameter, items2, open));
				sb.append("</div>");
			}
			sb.append("</div>");
		}
		return sb.toString();
	}

	private static boolean isRemarkEdit(final IAccount account, final QuestionRemarkItem item) {
		final boolean edit = account.getId().equals2(item.getUserId());
		/* 一天内允许编辑 */
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return edit && item.getCreateDate().after(cal.getTime());
	}
}
