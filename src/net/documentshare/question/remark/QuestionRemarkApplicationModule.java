package net.documentshare.question.remark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.documentshare.impl.AItSiteAppclicationModule;
import net.documentshare.impl.AbstractSendMail;
import net.documentshare.question.QuestionBean;
import net.documentshare.question.QuestionUtils;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.IInitializer;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.core.ado.db.Table;
import net.simpleframework.core.bean.IIdBeanAware;
import net.simpleframework.organization.IUser;
import net.simpleframework.web.page.PageRequestResponse;
import net.simpleframework.web.page.component.AbstractComponentBean;
import net.simpleframework.web.page.component.ComponentParameter;

public class QuestionRemarkApplicationModule extends AItSiteAppclicationModule implements IQuestionRemarkApplicationModule {
	private String deployName = "questionRemark";

	@Override
	public void init(IInitializer initializer) {
		try {
			super.init(initializer);
			doInit(QuestionRemarkUtils.class, deployName);
		} catch (Exception e) {
		}
	}

	static final Table question_remark = new Table("it_question_remark");

	@Override
	public AbstractComponentBean getComponentBean(PageRequestResponse requestResponse) {
		return null;
	}

	@Override
	public Class<? extends IIdBeanAware> getEntityBeanClass() {
		return QuestionRemarkItem.class;
	}

	@Override
	protected void putTables(final Map<Class<?>, Table> tables) {
		tables.put(QuestionRemarkItem.class, question_remark);
	}

	protected long getPostInterval() {
		return 1000 * 60;
	}

	public long getCount(final ComponentParameter compParameter) {
		return 0;
	}

	@Override
	public IDataObjectQuery<QuestionRemarkItem> getRemarkItems(final ComponentParameter compParameter) {
		return getRemarkItems(compParameter, null);
	}

	protected ExpressionValue getBeansSQL(final ComponentParameter compParameter, final Object parentId) {
		final ArrayList<Object> al = new ArrayList<Object>();
		final StringBuilder sql = new StringBuilder();
		final Object documentId = compParameter.getRequestParameter("questionId");
		sql.append("documentid=?");
		al.add(documentId);
		sql.append(" and ");
		if (parentId == null) {
			sql.append("parentid=0");
		} else {
			sql.append("parentid=?");
			al.add(parentId);
		}
		return new ExpressionValue(sql.toString(), al.toArray());
	}

	@Override
	public IDataObjectQuery<QuestionRemarkItem> getRemarkItems(final ComponentParameter compParameter, final Object parentId) {
		final ExpressionValue ev = getBeansSQL(compParameter, parentId);
		final ITableEntityManager tMgr = getDataObjectManager(QuestionRemarkItem.class);
		final IQueryEntitySet<QuestionRemarkItem> set = tMgr.query(new ExpressionValue(ev.getExpression()
				+ "  order by goodAnswer desc ,createdate desc", ev.getValues()), QuestionRemarkItem.class);
		set.getQueryDialect().setCountSQL(ev);
		return set;
	}

	@Override
	public QuestionRemarkItem doSupportOpposition(final ComponentParameter compParameter, final Object itemId, final boolean support) {
		final QuestionRemarkItem item = getBean(QuestionRemarkItem.class, itemId);
		if (item != null) {
			if (support) {
				item.setSupport(item.getSupport() + 1);
				doUpdate(new Object[] { "support" }, item);
			} else {
				item.setOpposition(item.getOpposition() + 1);
				doUpdate(new Object[] { "opposition" }, item);
			}
		}
		return item;
	}

	@Override
	public String getDeployPath() {
		return deployName;
	}

	@Override
	public void doRemarkSent(ComponentParameter compParameter, final QuestionRemarkItem item) {
		final QuestionBean questionBean = QuestionUtils.applicationModule.getBean(QuestionBean.class,item.getDocumentId());
		if (questionBean == null)
			return;
		doAttentionMail(compParameter, new AbstractSendMail(compParameter, getApplication()) {

			public IUser getUser() {
				return ItSiteUtil.getUserById(questionBean.getUserId());
			}

			@Override
			public boolean isSent(IUser toUser) {
				return !toUser.getId().equals2(item.getUserId());
			}

			@Override
			public String getSubject(final IUser toUser) {
				final StringBuilder sb = new StringBuilder();
				sb.append("[").append(questionBean.getTitle()).append("]").append("有回帖");
				return sb.toString();
			}

			@Override
			public String getBody(final IUser toUser) {
				final Map<String, Object> variable = new HashMap<String, Object>();
				variable.put("topicHref", linkUrl("/question/v/" + questionBean.getId() + ".html", null));
				variable.put("toUser", toUser.getText());
				variable.put("topic", questionBean.getTitle());
				variable.put("content", item.getContent());
				variable.put("fromUser", item.getUserText());
				return getFromTemplate(variable, QuestionRemarkApplicationModule.class, "question.html");
			}

			@Override
			public Object getId() {
				return questionBean.getId();
			}

		});
	}
}
