package net.documentshare.question;

import net.documentshare.question.remark.IQuestionRemarkApplicationModule;
import net.documentshare.question.remark.QuestionRemarkApplicationModule;
import net.simpleframework.core.AbstractInitializer;
import net.simpleframework.core.IApplication;
import net.simpleframework.core.IInitializer;

/**
 * 问答
 * @author lg
 *
 */
public class QuestionInitializer extends AbstractInitializer {
	private IQuestionApplicationModule questionApplicationModule;
	private IQuestionRemarkApplicationModule questionRemarkApplicationModule;

	@Override
	public void doInit(IApplication application) {
		try {
			super.doInit(application);
			questionApplicationModule = new QuestionApplicationModule();
			questionApplicationModule.init(this);
			questionRemarkApplicationModule = new QuestionRemarkApplicationModule();
			questionRemarkApplicationModule.init(this);
			IInitializer.Utils.deploySqlScript(getClass(), application, QuestionUtils.deployPath);
		} catch (Exception e) {
		}
	}
}
