package net.documentshare.question;

import java.util.Map;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class QuestionAttentionUsersPagerHandle extends AbstractPagerHandle {
	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, QuestionUtils.questionId);
		return parameters;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String questionId = compParameter.getRequestParameter(QuestionUtils.questionId);
		if (StringUtils.hasText(questionId)) {
			final ITableEntityManager tMgr = QuestionUtils.applicationModule.getDataObjectManager(QuestionAttention.class);
			return tMgr.query(new ExpressionValue(QuestionUtils.questionId + "=?", new Object[] { questionId }), QuestionAttention.class);
		} else {
			return null;
		}
	}
}
