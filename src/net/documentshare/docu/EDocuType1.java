package net.documentshare.docu;

public enum EDocuType1 {
	all() {
		@Override
		public String toString() {
			return "全部";
		}
	},
	docu() {
		@Override
		public String toString() {
			return "文档管理";
		}

		@Override
		public boolean isManager() {
			return true;
		}
	},
	remark() {
		@Override
		public String toString() {
			return "评论管理";
		}

		@Override
		public boolean isManager() {
			return true;
		}
	},
	file() {
		@Override
		public String toString() {
			return "附件管理";
		}

		@Override
		public boolean isManager() {
			return true;
		}

		@Override
		public String params() {
			return "__catalog_Id=-3";
		}
	};
	public boolean isManager() {
		return false;
	}

	public boolean isLogin() {
		return false;
	}

	public String params() {
		return "";
	}

}
