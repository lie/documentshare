package net.documentshare.docu;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 分类
 *
 */
public enum EDocuFunction {
	docu() {
		@Override
		public String toString() {
			return "文档";
		}
	},
	code() {
		public String toString() {
			return "代码";
		};
	},
	data() {
		public String toString() {
			return "资料";
		};
	},
	tool() {
		public String toString() {
			return "工具";
		};
	};

	public String toDescription() {
		return null;
	}

	public static EDocuFunction whichOrdinal(int o) {
		for (final EDocuFunction docu : EDocuFunction.values()) {
			if (docu.ordinal() == o) {
				return docu;
			}
		}
		return EDocuFunction.data;
	}

	public static EDocuFunction whichOne(final String name) {
		try {
			if ("all".equals(name)) {
				return EDocuFunction.docu;
			}
			return EDocuFunction.valueOf(name);
		} catch (Exception e) {
		}
		return EDocuFunction.data;
	}

	public static Map<Integer, String> functionMap() {
		final Map<Integer, String> dataMap = new LinkedHashMap<Integer, String>();
		dataMap.put(0, "所有");
		for (final EDocuFunction docu : EDocuFunction.values()) {
			dataMap.put(docu.ordinal() + 1, docu.toString());
		}
		return dataMap;
	}

}
