package net.documentshare.docu.corpus;

import java.util.Date;

import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;

public class CorpusOfDocBean extends AbstractIdDataObjectBean {
	private ID CorpusId;
	private ID docId;
	private Date createDate;

	public ID getCorpusId() {
		return CorpusId;
	}

	public void setCorpusId(ID corpusId) {
		CorpusId = corpusId;
	}

	public ID getDocId() {
		return docId;
	}

	public void setDocId(ID docId) {
		this.docId = docId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
