package net.documentshare.docu.all;

import java.util.ArrayList;
import java.util.Collection;

import net.documentshare.docu.DocuUtils;
import net.documentshare.docu.EDocuFunction;
import net.simpleframework.content.EContentStatus;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeBean;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeHandle;
import net.simpleframework.web.page.component.ui.tree.AbstractTreeNode;
import net.simpleframework.web.page.component.ui.tree.TreeNode;

public class AllDocuTreeHandle extends AbstractTreeHandle {
	@Override
	public Collection<? extends AbstractTreeNode> getTreenodes(final ComponentParameter compParameter, final AbstractTreeNode treeNode) {
		final Collection<AbstractTreeNode> nodeList = new ArrayList<AbstractTreeNode>();
		final AbstractTreeBean treeBean = (AbstractTreeBean) compParameter.componentBean;
		if (treeNode != null) {
			if ("allDocu".equals(treeNode.getId())) {
				treeNode.setImage(DocuUtils.deploy + "/images/cog.png");
				treeNode.setOpened(true);
				TreeNode treeNode2;
				for (final EDocuFunction docu : EDocuFunction.values()) {
					treeNode2 = new TreeNode(treeBean, treeNode, docu);
					treeNode2.setText("<span id='all" + docu.name() + "'>" + docu.toString() + "</span>");
					treeNode2.setId(docu.name());
					treeNode2.setJsClickCallback("refreshAllDocu('all" + docu.name() + "');$Actions['allDocuTableAct']('docu_type=" + docu.name()
							+ "');");
					treeNode2.setImage(DocuUtils.deploy + "/images/" + docu.name() + ".png");
					nodeList.add(treeNode2);
				}
				//
				treeNode2 = new TreeNode(treeBean, treeNode, "编辑中文档");
				treeNode2.setText("<span id='allNotEdit'>" + "编辑中文档" + "</span>");
				treeNode2.setId("allNotEdit");
				treeNode2.setJsClickCallback("refreshAllDocu('allNotEdit');$Actions['allDocuTableNonAct']('docu_status=" + EContentStatus.edit.name()
						+ "');");
				treeNode2.setImage(DocuUtils.deploy + "/images/edit.png");
				nodeList.add(treeNode2);
				//
				treeNode2 = new TreeNode(treeBean, treeNode, "审核中文档");
				treeNode2.setText("<span id='allNotAudit'>" + "审核中文档" + "</span>");
				treeNode2.setId("allNotAudit");
				treeNode2.setJsClickCallback("refreshAllDocu('allNotAudit');$Actions['allDocuTableNonAct']('docu_status="
						+ EContentStatus.audit.name() + "');");
				treeNode2.setImage(DocuUtils.deploy + "/images/audit.png");
				nodeList.add(treeNode2);
				//
				treeNode2 = new TreeNode(treeBean, treeNode, "用户下载");
				treeNode2.setText("<span id='allNotAudit'>" + "用户下载" + "</span>");
				treeNode2.setId("allNotDown");
				treeNode2.setJsClickCallback("refreshAllDocu('allNotDown');$Actions['docuDownAct']();");
				treeNode2.setImage(DocuUtils.deploy + "/images/down.png");
				nodeList.add(treeNode2);
			}
		} else {
			return super.getTreenodes(compParameter, treeNode);
		}
		return nodeList;
	}
}
