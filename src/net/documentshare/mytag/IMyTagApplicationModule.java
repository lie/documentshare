package net.documentshare.mytag;

import net.simpleframework.content.IContentApplicationModule;
import net.simpleframework.core.id.ID;
import net.simpleframework.web.IWebApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;

public interface IMyTagApplicationModule extends IWebApplicationModule {

	static final String _CATALOG_ID = "catalogId";

	static final String _TAG_ID = "tagId";

	String getTagUrl(PageRequestResponse requestResponse, MyTagBean tag);

	IContentApplicationModule getContentApplicationModule(PageRequestResponse requestResponse);

	ID getCatalogId(final PageRequestResponse requestResponse);

	void reCreateAllTags(final PageRequestResponse requestResponse);
}
