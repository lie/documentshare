package net.documentshare.mytag;

import java.util.Map;

import net.documentshare.docu.DocuUtils;
import net.documentshare.docu.IDocuApplicationModule;
import net.simpleframework.ado.DataObjectManagerUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.IQueryEntitySet;
import net.simpleframework.applets.tag.ITagApplicationModule;
import net.simpleframework.content.IContentApplicationModule;
import net.simpleframework.content.bbs.IBbsApplicationModule;
import net.simpleframework.content.component.catalog.Catalog;
import net.simpleframework.core.IInitializer;
import net.simpleframework.core.ado.db.Table;
import net.simpleframework.core.bean.IIdBeanAware;
import net.simpleframework.core.id.ID;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.AbstractWebApplicationModule;
import net.simpleframework.web.WebUtils;
import net.simpleframework.web.page.PageRequestResponse;

public class DefaultMyTagApplicationModule extends AbstractWebApplicationModule implements IMyTagApplicationModule {

	static final String deployName = "mytag";

	@Override
	public void init(final IInitializer initializer) {
		super.init(initializer);
		doInit(MyTagUtils.class, deployName);
	}

	@Override
	protected void putTables(final Map<Class<?>, Table> tables) {
		tables.put(MyTagBean.class, new Table("it_mytag"));
		tables.put(MyTagRBean.class, new Table("it_myrtag", new String[] { "tagid", "rid" }));
	}

	@Override
	public Class<? extends IIdBeanAware> getEntityBeanClass() {
		return MyTagBean.class;
	}

	@Override
	public IContentApplicationModule getContentApplicationModule(final PageRequestResponse requestResponse) {
		final ETagType vtype = MyTagUtils.getVtype(requestResponse);
		if (vtype == ETagType.docu) {
			return DocuUtils.applicationModule;
		} else {
			return null;
		}
	}

	public String getManager(final Object... params) {
		final PageRequestResponse requestResponse = (PageRequestResponse) params[0];
		final IContentApplicationModule module = getContentApplicationModule(requestResponse);
		if (module != null) {
			return module.getManager(requestResponse);
		}
		return super.getManager(requestResponse);
	}

	@Override
	public ID getCatalogId(final PageRequestResponse requestResponse) {
		final IContentApplicationModule module = getContentApplicationModule(requestResponse);
		ID id = null;
		if (module instanceof IBbsApplicationModule) {
			id = DataObjectManagerUtils.getTableEntityManager(module, Catalog.class).getTable()
					.newID(requestResponse.getRequestParameter(ITagApplicationModule._CATALOG_ID));
		} else if (module instanceof IDocuApplicationModule) {
			id = DataObjectManagerUtils.getTableEntityManager(module, Catalog.class).getTable()
					.newID(requestResponse.getRequestParameter(ITagApplicationModule._CATALOG_ID));
		}
		return id == null ? ID.zero : id;
	}

	@Override
	public String getTagUrl(final PageRequestResponse requestResponse, final MyTagBean tag) {
		final IContentApplicationModule module = getContentApplicationModule(requestResponse);
		if (module instanceof IDocuApplicationModule) {
			return WebUtils.addParameters(
					module.getApplicationUrl(requestResponse),
					"t=" + StringUtils.text(requestResponse.getRequestParameter("t"), "0") + "&" + _TAG_ID + "=" + tag.getId() + "&s="
							+ StringUtils.text(requestResponse.getRequestParameter("s"), "1") + "&od="
							+ StringUtils.text(requestResponse.getRequestParameter("od"), "0"));
		} else {
			return null;
		}
	}

	@Override
	public void reCreateAllTags(final PageRequestResponse requestResponse) {
		final IContentApplicationModule module = getContentApplicationModule(requestResponse);
		if (module != null) {
			final ETagType vtype = MyTagUtils.getVtype(requestResponse);
			final ID catalogId = getCatalogId(requestResponse);
			ExpressionValue ev = null;
			final IQueryEntitySet<Map<String, Object>> qs = DataObjectManagerUtils.getTableEntityManager(module).query(
					new String[] { "id", "keywords" }, ev);
			Map<String, Object> map;
			while ((map = qs.next()) != null) {
				final String keywords = (String) map.get("keywords");
				if (StringUtils.hasText(keywords)) {
					MyTagUtils.syncTags(vtype, catalogId, keywords, ID.Utils.newID(map.get("id")));
				}
			}
			MyTagUtils.doTagsRebuild(vtype, catalogId);
		}
	}

}
