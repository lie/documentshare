package net.documentshare.mytag;

import java.util.List;
import java.util.Map;

import net.simpleframework.web.page.DefaultPageHandle;
import net.simpleframework.web.page.PageParameter;

public class MyTagEditPageLoad extends DefaultPageHandle {

	public void optionLoad(final PageParameter pageParameter, final Map<String, Object> dataBinding, final List<String> visibleToggleSelector,
			final List<String> readonlySelector, final List<String> disabledSelector) {
		final MyTagBean tag = MyTagUtils.getTableEntityManager().queryForObjectById(
				pageParameter.getRequestParameter(IMyTagApplicationModule._TAG_ID), MyTagBean.class);
		if (tag != null) {
			dataBinding.put("to_type", tag.getTtype());
		}
	}
}
