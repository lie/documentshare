package net.documentshare.documentconfig;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.documentshare.utils.Dom4jUtils;
import net.documentshare.utils.UID;
import net.simpleframework.util.StringUtils;

import org.dom4j.Document;
import org.dom4j.Element;

/**
 * 文档管理器
 *
 */
public class DocumentConfigMgr {
	private static DocumentConfigMgr docuMgr = new DocumentConfigMgr();
	private Map<String, ValueBean> valuesMap = new HashMap<String, ValueBean>();
	private Map<String, StorageBean> storageMap = new LinkedHashMap<String, StorageBean>();
	private List<StorageBean> localList = new ArrayList<StorageBean>();
	private List<StorageBean> ftpList = new ArrayList<StorageBean>();
	private StorageBean storageBean;
	private String filePath;
	private Document dom;
	private Element rootEle;
	private Element storageEle;
	private Element valuesEle;

	private DocumentConfigMgr() {
	}

	public List<StorageBean> getLocalList() {
		return localList;
	}

	public List<StorageBean> getStorageList() {
		final List<StorageBean> list = new ArrayList<StorageBean>();
		list.addAll(localList);
		list.addAll(ftpList);
		return list;
	}

	public List<StorageBean> getFtpList() {
		return ftpList;
	}

	public StorageBean getStorageBean() {
		return storageBean;
	}

	public Map<String, StorageBean> getStorageMap() {
		return storageMap;
	}

	public void saveDocuMgr() {
		try {
			Dom4jUtils.saveDom4jXmlDocument(filePath, dom);
		} catch (Exception e) {
		}
	}

	public void currentStorage(final String id) {
		if (storageMap.get(id) != null) {
			this.storageBean = storageMap.get(id);
			this.storageEle.addAttribute("dd", id);
			saveDocuMgr();
		}
	}

	public boolean removeStorageBean(final String id) {
		final StorageBean storageBean = storageMap.get(id);
		if (storageBean.canDelete()) {
			storageEle.remove(storageBean.getEle());
			ftpList.remove(storageBean);
			localList.remove(storageBean);
			saveDocuMgr();
			return true;
		}
		return false;
	}

	public StorageBean createStorageBean(final String type) {
		StorageBean storageBean;
		if ("local".equals(type)) {
			storageBean = new LocalStorageBean(storageEle.addElement("s"));
			localList.add(storageBean);
		} else {
			storageBean = new FtpStorageBean(storageEle.addElement("s"));
			ftpList.add(storageBean);
		}
		if (storageBean != null) {
			storageBean.setId(UID.asString());
			storageMap.put(storageBean.getId(), storageBean);
		}
		return storageBean;
	}

	public void initDocuMgr(final String path) {
		try {
			filePath = path;
			dom = Dom4jUtils.createDocument(new File(path));
			rootEle = dom.getRootElement();
			this.valuesEle = rootEle.element("values");
			final List<Element> valuesEle = this.valuesEle.elements("value");
			for (final Element ele : valuesEle) {
				docuMgr.valuesMap.put(ele.attributeValue("id"), new ValueBean(ele));
			}
			this.storageEle = rootEle.element("storage");
			final List<Element> sEle = this.storageEle.elements("s");
			final String dd = this.storageEle.attributeValue("dd");
			StorageBean storageBean = null;
			for (final Element ele : sEle) {
				if (StringUtils.hasText(ele.attributeValue("host"))) {
					storageBean = new FtpStorageBean(ele);
					ftpList.add(storageBean);
				} else {
					storageBean = new LocalStorageBean(ele);
					localList.add(storageBean);
				}
				if (storageBean.getId().equals(dd)) {
					this.storageBean = storageBean;
				}
				storageMap.put(storageBean.getId(), storageBean);
			}
			saveDocuMgr();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DocumentConfigMgr getDocuMgr() {
		return docuMgr;
	}

	public void putValue(final String key, final String value) {
		final ValueBean valueBean = valuesMap.get(key);
		if (valueBean != null) {
			valueBean.setValue(value);
		}
	}

	public String getValue(final String key) {
		final ValueBean valueBean = valuesMap.get(key);
		if (valueBean == null)
			return "";
		return valueBean.value;
	}

	public static class ValueBean {
		public Element ele;
		public String value;

		public ValueBean(Element ele) {
			this.ele = ele;
			this.value = ele.getTextTrim();
		}

		public void setValue(final String value) {
			this.value = value;
			this.ele.setText(value);
		}
	}

	/**
	 * 返回可用磁盘Key
	 * @return
	 */
	public String getUsableDatabase() {
		if (this.storageBean.hasFreeSpace()) {
			return this.storageBean.getId();
		}
		for (final String key : this.storageMap.keySet()) {
			final StorageBean sb = this.storageMap.get(key);
			if (sb.hasFreeSpace()) {
				this.storageBean = sb;
				saveDocuMgr();
				break;
			}
		}
		return this.storageBean.getId();
	}
}
