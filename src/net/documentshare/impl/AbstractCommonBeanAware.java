package net.documentshare.impl;

import java.util.Calendar;
import java.util.Date;

import net.documentshare.i.ICommonBeanAware;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.IUser;
import net.simpleframework.util.IConstants;

public abstract class AbstractCommonBeanAware extends AbstractIdDataObjectBean implements ICommonBeanAware {
	private ID userId;// 发布者
	private ID lastUserId;// 修改者
	private String content;// 内容描述
	private long attentions;// 关注
	private long remarks;// 评论数
	private long views;// 总访问量
	private long todayViews;// 今日访问量
	private boolean ttop = false;// 置顶
	private long oorder;// 排序
	private ID remarkUserId;//最后发评论的用户
	private Date remarkDate;//最后发评论的时间
	private Date createDate;// 发布日期
	private Date modifyDate;//修改时间
	private long votes;

	public AbstractCommonBeanAware() {
		this.createDate = new Date();
		this.modifyDate = new Date();
		this.remarkDate = new Date();
	}

	public void setVotes(long votes) {
		this.votes = votes;
	}

	public long getVotes() {
		return votes;
	}

	public ID getLastUserId() {
		return lastUserId;
	}

	public void setLastUserId(ID lastUserId) {
		this.lastUserId = lastUserId;
	}

	public void setRemarkUserId(ID remarkUserId) {
		this.remarkUserId = remarkUserId;
	}

	public ID getRemarkUserId() {
		return remarkUserId;
	}

	public void setRemarkDate(Date remarkDate) {
		this.remarkDate = remarkDate;
	}

	public Date getRemarkDate() {
		return remarkDate;
	}

	/**
	 * 是不是有新的评论
	 * @return
	 */
	public boolean isNewRemark() {
		if (remarkDate == null)
			return false;
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR, -12);
		if (remarkDate.after(cal.getTime())) {
			return true;
		}
		return false;
	}

	public void setTodayViews(long todayViews) {
		this.todayViews = todayViews;
	}

	public long getTodayViews() {
		return todayViews;
	}

	public void setTtop(boolean ttop) {
		this.ttop = ttop;
	}

	public boolean isTtop() {
		return ttop;
	}

	@Override
	public void setOorder(long oorder) {
		this.oorder = oorder;
	}

	@Override
	public long getOorder() {
		return oorder;
	}

	public String getRemarkUserText() {
		final IUser user = ItSiteUtil.getUserById(getRemarkUserId());
		return user != null ? user.getText() : IConstants.HTML_BLANK_STRING;
	}

	@Override
	public String getUserText() {
		final IUser user = ItSiteUtil.getUserById(getUserId());
		return user != null ? user.getText() : IConstants.HTML_BLANK_STRING;
	}

	public ID getUserId() {
		return userId;
	}

	public void setUserId(ID userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getAttentions() {
		return attentions;
	}

	public void setAttentions(long attentions) {
		this.attentions = attentions;
	}

	public long getRemarks() {
		return remarks;
	}

	public void setRemarks(long remarks) {
		this.remarks = remarks;
	}

	public long getViews() {
		return views;
	}

	public void setViews(long views) {
		this.views = views;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String[] getRemarkBean() {
		return new String[] { "remarks", "remarkUserId", "remarkDate" };
	}
}
