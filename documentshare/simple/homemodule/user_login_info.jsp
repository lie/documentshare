<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.organization.component.login.LoginUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.simpleframework.organization.IUser"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IUser user = ItSiteUtil.getLoginUser(requestResponse);
%>
<style>
.login_module {
	display: inline-block;
	border: 1px solid #DDD;
	margin: 4px 4px 4px 0px;
	padding: 4px 10px;
	background: #F4F9FF;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	border-radius: 2px;
	text-decoration: none;
}

.login_module:hover {
	background: #F4FAFF;
	text-decoration: none;
}
</style>
<div>
	<table>
		<tr>
			<td valign="top"><%=ContentUtils.getAccountAware().wrapImgAccountHref(requestResponse, user,80,80)%></td>
			<td valign="top">
				<table>
					<tr>
						<td>
							你好,<%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, user)%></td>
					</tr>
					<tr>
						<td>
							<a class="login_module" href="/space.html?t=document&id=myUpload">上传文档</a><a
								class="login_module"
								onclick="$Actions['editUserWindow']('userid=<%=user.getId()%>');">个人资料</a><a
								class="login_module" href="/home.html">个性主页</a><a
								class="login_module"
								href="/space.html?t=document&id=myAttention">我的收藏</a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>