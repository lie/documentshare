<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.IWebApplicationModule"%>
<%@page import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.question.QuestionUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String appUrl = QuestionUtils.applicationModule.getApplicationUrl(requestResponse);
	final String t = request.getParameter("t");
%>
<div id="__barHeader"></div>
<div class="simple_toolbar2">
	<table width="100%" cellpadding="1" cellspacing="0">
		<tr>
			<td width="100%">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="left">
							<input type="button" class="button2"
								onclick="$IT.A('questionAddWindowAct');" value="发布问答">
							<%
								if (ItSiteUtil.isManage(requestResponse, QuestionUtils.applicationModule)) {
									out.write("<a class=\"a2\" style=\"margin-left: 10px; vertical-align: middle;\" onclick=\"$IT.A('questionMgrToolsWindowAct');\">管理员工具</a>");
								}
							%>
						</td>
						<td id="__blog_sech" align="right"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%"><%=QuestionUtils.applicationModule.tabs(requestResponse)%></td>
		</tr>
		<tr>
			<td align="right">
				<%=QuestionUtils.applicationModule.tabs13(requestResponse)%>&nbsp;
			</td>
		</tr>
	</table>
</div>

<script type="text/javascript">
(function() {
		var sb = $Comp.searchButton(function(sp) {
			$Actions.loc("<%=appUrl%>".addParameter("t=<%=t%>&c=" + $F(sp.down(".txt"))));
		}, function(sp) {
			$('sech_pane_params').$toggle();
		}, null, 210);
		$("__blog_sech").update(sb);
		<%final String c = WebUtils.toLocaleString(request.getParameter("c"));
			if (StringUtils.hasText(c)) {%>
			sb.down(".txt").setValue("<%=c%>");
		<%}%>
	}
	)();
</script>