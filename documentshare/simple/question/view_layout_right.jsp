<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%><%@page
	import="net.documentshare.question.QuestionBean"%><%@page
	import="net.documentshare.question.QuestionUtils"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div class="block_layout1">
	<div class="t f4">
		相关推荐
	</div>
	<div class="wrap_text">
		<%
			final QuestionBean osBean = QuestionUtils.applicationModule.getViewQuestionBean(requestResponse);
			request.setAttribute("__qs", QuestionUtils.applicationModule.createLuceneManager(new ComponentParameter(request, response, null))
					.getLuceneQuery(osBean == null ? "" : osBean.getTitle()));
		%>
		<jsp:include page="view_layout_show.jsp" flush="true">
			<jsp:param value="false" name="showMore" />
			<jsp:param value="dot1" name="dot" />
			<jsp:param value="20" name="rows" />
		</jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		其他问答
	</div>
	<div class="wrap_text">
		<%
			request.setAttribute("__qs", QuestionUtils.queryQuestion(requestResponse, "other"));
		%>
		<jsp:include page="view_layout_show.jsp" flush="true">
			<jsp:param value="false" name="showMore" />
			<jsp:param value="dot1" name="dot" />
			<jsp:param value="6" name="rows" />
		</jsp:include></div>
</div>
