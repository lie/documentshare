<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%
	final String p = "/simple/main"
			+ StringUtils.blank(request.getParameter("p")) + ".jsp";
%>
<jsp:include page="/simple/template/c.jsp" flush="true">
	<jsp:param value="<%=p%>" name="center" />
</jsp:include>