<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<html>
<body style="overflow: hidden;">
	<%
		String left = request.getParameter("left");
		String center = request.getParameter("center");
	%>
	<jsp:include page="header.jsp" flush="true"></jsp:include>
	<div id="t_main">
		<table class="fixed_table" style="width: 100%; height: 100%;"
			cellpadding="0" cellspacing="0">
			<tr>
				<td width="20%" valign="top">
					<div class="left">
						<%
							if (StringUtils.hasText(left)) {
						%><jsp:include page="<%=left%>" flush="true"></jsp:include>
						<%
							}
						%>
					</div>
				</td>
				<td class="splitbar"></td>
				<td valign="top">
					<div class="center">
						<%
							if (StringUtils.hasText(center)) {
						%><jsp:include page="<%=center%>" flush="true"></jsp:include>
						<%
							}
						%>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<jsp:include page="footer.jsp" flush="true"></jsp:include>
</body>
<script type="text/javascript">
	(function() {
		var sb = $$("#t_main .splitbar")[0];
		$Comp.createSplitbar(sb, sb.previous());
	})();
</script>
</html>