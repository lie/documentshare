<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="docuForm">
	<input type="hidden" id="docuId" name="docuId">
	<table width="100%'">
		<tbody>
			<tr>
				<td class="l">
					标题：
				</td>
				<td>
					<input type="text" id="docu_title" name="docu_title"
						style="width: 90%" value="">
				</td>
			</tr>
			<tr>
				<td class="l" valign="top">
					描述：
				</td>
				<td>
					<textarea id="docu_description" name="docu_description" rows="5"
						style="width: 90%"></textarea>
				</td>
			</tr>
			<tr>
				<td class="l">
					关键字：
				</td>
				<td>
					<input type="text" id="docu_keyworks" name="docu_keyworks"
						style="width: 90%">
				</td>
			</tr>
			<tr>
				<td class="l">
					分类：
				</td>
				<td>
					<div id="td_docu_catalog" style="width: 90%"></div>
					<input type="hidden" name="docu_catalog" id="docu_catalog">
				</td>
			</tr>
			<tr>
				<td class="l">
					价格：
				</td>
				<td>
					<input type="radio" checked="checked" id="docu_free_yes"
						name="docu_free" onclick="$('docu_point').value = 0;">
					<label for="docu_free_yes">
						免费
					</label>
					<input type="radio" id="docu_free_no" name="docu_free">
					<select id="docu_point" name="docu_point">
						<option value="0">
							0
						</option>
						<option value="1">
							1
						</option>
						<option value="2">
							2
						</option>
						<option value="5">
							5
						</option>
						<option value="10">
							10
						</option>
						<option value="15">
							15
						</option>
						<option value="20">
							20
						</option>
					</select>
					免费阅读页数
					<select id="docu_free_page" name="docu_free_page">
						<option value="0">
							0
						</option>
						<option value="1">
							1
						</option>
						<option value="2">
							2
						</option>
						<option value="5">
							5
						</option>
						<option value="10">
							10
						</option>
						<option value="15">
							15
						</option>
						<option value="20">
							20
						</option>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div align="center">
	<input type="button" class="button2"
		onclick="$IT.A('docuEditSaveAct');" value="提交文档">
</div>
<style type="text/css">
#docuForm .l {
	width: 70px;
	text-align: right;
}

#docuForm {
	background: #f7f7ff;
	border: 1px solid #ddd;
	padding: 6px 8px;
}

#docuForm .ccontent {
	border-bottom: 1px dotted #ccc;
}
</style>
<script type="text/javascript">
$ready(function() {
	addTextButton("docu_catalog_text", "docuCatalogAct", "td_docu_catalog",
			false);
});
</script>