<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.util.StringUtils"%>
<%@page import="net.simpleframework.content.EContentType"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.documentshare.docu.EDocuStatus"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div id="__docuAttrForm2">
	<input type="hidden" name="docuId" id="docuId" />
	<table width="100%" cellpadding="2" cellspacing="0">
		<tr>
			<td class="l">
				类型
			</td>
			<td>
				<select id="docu_ttype" name="docu_ttype">
					<option value="<%=EContentType.normal.name()%>"><%=EContentType.normal%></option>
					<option value="<%=EContentType.recommended.name()%>"><%=EContentType.recommended%></option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="l">
				状态
			</td>
			<td>
				<select id="docu_status" name="docu_status">
					<option value="<%=EDocuStatus.audit.name()%>"><%=EDocuStatus.audit%></option>
					<option value="<%=EDocuStatus.publish.name()%>"><%=EDocuStatus.publish%></option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="l">
				评分
			</td>
			<td>
				<input type="text" id="docu_grade" name="docu_grade"
					style="width: 48px;">
				最高5分,最低0
			</td>
		</tr>
	</table>
</div>
<div style="text-align: right; margin-top: 6px;">
	<input type="button" class="button2" value="保存"
		onclick="$IT.A('docuAttrSaveAct');" />
	<input type="button" value="取消" onclick="$IT.C('docuAttrWindowAct');" />
</div>
<style type="text/css">
#__docuAttrForm2 .l {
	width: 70px;
	text-align: right;
}

#__docuAttrForm2 {
	background: #f7f7ff;
	border: 1px solid #ddd;
	padding: 6px 8px;
}
</style>
