<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.simpleframework.util.ConvertUtils"%>
<%@page import="net.simpleframework.organization.account.IAccount"%>
<%@page import="net.simpleframework.organization.OrgUtils"%>
<%@page import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final DocuBean docuBean = DocuUtils.applicationModule.getViewDocuBean(requestResponse);
%>
<div class="block_layout1" align="center">
	<input type="button" class="button2" value="侵犯版权，我要投诉"
		onclick="$Actions.loc('/guestbook.html?docuId=<%=docuBean.getId() %>');">
</div>
<div class="block_layout1">
	<div class="t f4">
		相关文档
	</div>
	<div class="wrap_text">
		<%
			request.setAttribute("__qs", DocuUtils.applicationModule.createLuceneManager(new ComponentParameter(request, response, null))
					.getLuceneQuery(docuBean == null ? "" : docuBean.getTitle()));
		%>
		<jsp:include page="view_layout_show.jsp" flush="true">
			<jsp:param value="false" name="showMore" />
			<jsp:param value="dot1" name="dot" />
			<jsp:param value="6" name="rows" />
		</jsp:include></div>
</div>
<div class="block_layout1">
	<div class="t f4">
		同类文档
	</div>
	<div class="wrap_text">
		<%
			request.setAttribute("__qs", DocuUtils.queryDocu(requestResponse, docuBean,"", "same"));
		%>
		<jsp:include page="view_layout_show.jsp" flush="true">
			<jsp:param value="false" name="showMore" />
			<jsp:param value="dot1" name="dot" />
			<jsp:param value="6" name="rows" />
		</jsp:include></div>
</div>
