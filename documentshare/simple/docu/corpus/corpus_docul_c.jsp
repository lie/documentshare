<%@page import="net.simpleframework.organization.account.AccountSession"%>
<%@page import="net.documentshare.docu.corpus.CorpusUtils"%>
<%@page import="net.documentshare.impl.AbstractAttention"%>
<%@page import="net.simpleframework.organization.account.IAccount"%>
<%@page import="net.simpleframework.util.StringUtils"%>
<%@page import="net.documentshare.utils.DateUtils"%>
<%@page import="net.documentshare.docu.DocuUtils"%>
<%@page import="net.documentshare.docu.corpus.CorpusBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String id = request.getParameter("corpusId");
	String setFront = request.getParameter("setFront");
	CorpusBean bean = CorpusUtils.addViews(id);
	final IAccount account = AccountSession.getLogin(request.getSession());
	AbstractAttention docuAttention = null;
	String text = "收藏文辑";
	if (account != null) {
		docuAttention = CorpusUtils.getAttention(account.getId(), bean.getId());
	}
	if (docuAttention != null) {
		text = "取消收藏";
	}
%>
<div style="margin-bottom: 6px;" class="common_catalog" id="setFontPage">
	<div class="c">
		<div class="ctitle ctitle_border_radius"
			style="padding: 16px; line-height: 0px;">
			<div style="float: left;"><%=bean.getName()%></div>
			<div style="float: right;">
				<%
					if (account != null) {
				%>
				<a class="a2" id="__corpus_attention"
					onclick="$Actions['corpusAttentionAct']('corpusId=<%=bean.getId().getValue()%>')"><%=text%></a>|
				(<a class="a2" id="__attentionUsersWindow"
					onclick="$Actions['attentionUsersWindowAct']('corpusId=<%=bean.getId()%>');"><%=bean.getAttentions()%></a>)
				<%
					}
				%>
				阅读:<%=bean.getReadTime()%>|创建:<%=DateUtils.formatDate(bean.getCreateDate(), DateUtils.DEFAULT_DATE_PATTERN)%>
			</div>
		</div>
		<div id="MyCorposDocList" style="padding: 0px 10px 0px 10px;"></div>
		<%
			if (StringUtils.hasText(setFront)) {
		%>
		<div style="padding: 10px;">
			<input type="button" name="setFontButton"
				onclick="$Actions['setFrontAjax']();" value="提交修改">
		</div>
		<%
			}
		%>
	</div>
</div>
