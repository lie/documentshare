<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.mytag.ETagType"%><%@page
	import="net.documentshare.mytag.MyTagUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%>
<div class="block_layout1">
	<div class="t f4">
		最新文辑
	</div>
	<div class="wrap_text">
		<jsp:include page="corpus_layout_show.jsp" flush="true">
			<jsp:param value="newest" name="type" />
		</jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		热门文辑
	</div>
	<div class="wrap_text">
		<jsp:include page="corpus_layout_show.jsp" flush="true">
			<jsp:param value="viewest" name="type" />
		</jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		推荐文辑
	</div>
	<div class="wrap_text">
		<jsp:include page="corpus_layout_show.jsp" flush="true">
			<jsp:param value="recommend" name="type" />
		</jsp:include>
	</div>
</div>
