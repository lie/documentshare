<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.simpleframework.core.ado.IDataObjectQuery"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="java.util.Map"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = DocuUtils.queryDocuByDownload(requestResponse);
	if (qs == null) {
		return;
	}
%>
<div class="list_layout">
	<%
		Map<String, Object> map;
		while ((map = (Map<String, Object>) qs.next()) != null) {
			if (qs.position() > 9) {
				break;
			}
			DocuBean docuBean = DocuUtils.applicationModule.getBean(DocuBean.class, map.get("docuId"));
			if (docuBean == null) {
				continue;
			}
	%>
	<div class="rrow" style="padding-left: 0px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td width="20" valign="top">
					<img alt="<%=docuBean.getExtension()%>"
						src="<%=DocuUtils.getFileImage(requestResponse, docuBean)%>">
				</td>
				<td><%=DocuUtils.wrapOpenLink(requestResponse, docuBean)%></td>
				<td align="right" class="nnum" valign="top">
					<span title="<%=docuBean.getDownCounter()%>下载"><%=docuBean.getDownCounter()%></span>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>