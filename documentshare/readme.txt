需要安装
1、文档转换工具
第一种：OpenOffice.org 3.3
http://10.10.4.8/download/4480282/5452666/1/exe/175/117/1304550751663_629/OOo_3.3.0_Win_x86_install_zh-CN.exe
第二种:LibO_3.4.4
http://mirror.bjtu.edu.cn/tdf/libreoffice/stable/3.4.4/win/x86/LibO_3.4.4_Win_x86_install_multi.exe

配置：/documentshare/config/docviewer.properties
docviewer.converter.office.home=这里配置文档转换工具的路径

2、swf转换工具
swftools
这个工具你发给过我们，你应该有。
配置：/documentshare/config/docviewer.properties
docviewer.converter.pdf.command=第一个路径是swf转换的路径，第二个路径是字符集的路
3、字符集路径配置，用户转换swf用
xpdf/xpdf-chinese-simplified/add-to-xpdfrc
4、磁盘存储路径设置
配置：/documentshare/config/document.txt
5、project.xml
6、account_rule.xml积分规则