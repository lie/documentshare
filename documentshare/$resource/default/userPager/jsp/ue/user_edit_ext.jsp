<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.organization.OrgUtils"%>

<form id="_userattr_form">
	<input type="hidden" id="<%=OrgUtils.um().getUserIdParameterName()%>"
		name="<%=OrgUtils.um().getUserIdParameterName()%>"
		value="<%=StringUtils.blank(request.getParameter(OrgUtils.um().getUserIdParameterName()))%>" />
	<table style="width: 100%;" cellpadding="2" cellspacing="0">
		<tr>
			<td align="right">
				<input type="button" class="button2" id="_userattr_save"
					onclick="$Actions['ajaxEditUserAttr']();"
					value="#(user_edit_base.1)" />
				<input type="button" value="#(Button.Cancel)"
					onclick="$Actions['editUserWindow'].close();" />
			</td>
		</tr>
		<tr>
			<td>
				<div id="_userattr_0"></div>
			</td>
		</tr>
	</table>
</form>